package ExercíciosPropostos;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Capítulo1Test {

    @Test
    void cambiarTaxaEuroParaDolar() {
        Capítulo1 c = new Capítulo1();
        double result = c.cambiarTaxaMoedas(3,1.12);
        assertEquals(3.36, result);
    }
    @org.junit.jupiter.api.Test
    void cambiarTaxaDolarParaLibra() {
        Capítulo1 c = new Capítulo1();
        double result = c.cambiarTaxaMoedas(3,0.77);
        assertEquals(2.31, result);
    }
    @org.junit.jupiter.api.Test
    void cambiarTaxaLibraParaEuro() {
        Capítulo1 c = new Capítulo1();
        double result = c.cambiarTaxaMoedas(3,1.16);
        assertEquals(3.48, result);
    }
    @Test
    void cambiarEuroParaDólar() {
        Capítulo1 c = new Capítulo1();
        double result = c.cambiarMoedas("Euro", "Dólar", 3);
        assertEquals(3.36, result);
    }
    @Test
    void cambiarDólarParaEuro() {
        Capítulo1 c = new Capítulo1();
        double result = c.cambiarMoedas("Dólar", "Euro", 5);
        assertEquals(4.5, result);
    }
    @Test
    void disteuclidiana1() {
        Capítulo1 c = new Capítulo1();
        double result = c.disteuclidiana(3,2,5,6);
        assertEquals(4.47, result);
    }
    @Test
    void disteuclidiana2() {
        Capítulo1 c = new Capítulo1();
        double result = c.disteuclidiana(3,2,5,7);
        assertEquals(5.39, result);
    }
    @Test
    void disteuclidiana3() {
        Capítulo1 c = new Capítulo1();
        double result = c.disteuclidiana(0,0,0,0);
        assertEquals(0, result);
    }
    /*@Test
    void areaPerimetro1() {
        Capítulo1 c = new Capítulo1();
        String [][] result = c.areaPerimetro(5);
        assertArrayEquals(new String [][]{{"Área:", "78.54"},{"Perímetro", "31.42"}});
    }*/
}