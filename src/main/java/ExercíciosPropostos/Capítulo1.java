package ExercíciosPropostos;

public class Capítulo1 {

    public double cambiarTaxaMoedas (double moedaOrig, double taxaC) {
        double moeda2 = moedaOrig*taxaC;
        double moedaFinal = (double) Math.round(moeda2*100)/100;
        return moedaFinal;
    }

    public double cambiarMoedas (String Camb1, String Camb2, double moedaOrig) {
        double moedaFinal = 0;
        if (Camb1.equals("Euro") && Camb2.equals("Dólar")) {
            double moeda2 = moedaOrig*1.12;
            moedaFinal = (double) Math.round(moeda2*100)/100;
        }
        if (Camb1.equals("Dólar") && Camb2.equals("Euro")) {
            double moeda2 = moedaOrig*0.90;
            moedaFinal = (double) Math.round(moeda2*100)/100;
        }
        return moedaFinal;
    }

    public double disteuclidiana (int x1, int y1, int x2, int y2) {
        double distancia = Math.sqrt((Math.pow((x2-x1),2)+Math.pow((y2-y1),2)));
        double distanciaFinal = (double) Math.round(distancia*100)/100;
        return distanciaFinal;
    }

    public String[][] areaPerimetro (double raio) {
        double area = Math.round((Math.PI*Math.pow(raio,2))*100)/100;
        double perimetro = Math.round((2*Math.PI*raio)*100)/100;
        String [][] matrix = new String [2][2];
        matrix[0][0]= "Área:";
        matrix[0][1]= Double.toString(area);
        matrix[1][0]= "Perímetro:";
        matrix[1][1]= Double.toString(perimetro);
        return matrix;
    }

}